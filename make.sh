#!/usr/bin/bash

clang-11 -Ofast -march=native -S -emit-llvm rts.c -o rts.ll
llvm-link-11 rts.ll $1 -o program.bc
clang-11 -Ofast -march=native program.bc -o $(basename -s ".ll" $1).out
