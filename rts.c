#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

int64_t B[1024];
size_t bsp = -1;

void start(void);

bool peek_b_b(void) {
	return B[bsp--] != 0;
}

void eqI_b(int64_t element, int64_t offset) {
	size_t target = bsp - offset;
	if (B[target] == element) {
		B[++bsp] = 1;
	} else {
		B[++bsp] = 0;
	}
}

void pop_b(int64_t offset) {
	bsp -= offset;
}

void pushI(int64_t element) {
	B[++bsp] = element;
}

void push_b(int64_t offset) {
	int64_t element = B[bsp - offset];
	B[++bsp] = element;
}

void subI(void) {
	int64_t op1 = B[bsp--];
	int64_t op2 = B[bsp];
	B[bsp] = op1 - op2;
}

void addI(void) {
	int64_t op1 = B[bsp--];
	int64_t op2 = B[bsp];
	B[bsp] = op1 + op2;
}

void update_b(int64_t offset_f, int64_t offset_t) {
	size_t target_f = bsp - offset_f;
	size_t target_t = bsp - offset_t;
	B[target_t] = B[target_f];
}

void updatepop_b(int64_t offset_f, int64_t offset_t) {
	size_t target_f = bsp - offset_f;
	size_t target_t = bsp - offset_t;
	B[target_t] = B[target_f];
	bsp = target_t;
}

int main(void) {
	start();
	printf("Result: %ld\n", B[0]);
	return 1;
}
