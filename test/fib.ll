declare void @eqI_b(i64, i64)
declare i1 @peek_b_b()
declare void @pop_b(i64)
declare void @pushI(i64)
declare void @push_b(i64)
declare void @subI()
declare void @update_b(i64, i64)
declare void @updatepop_b(i64, i64)
declare void @addI()

define void @start() {
l0:
	call void @pushI(i64 45)
	call void @s1()
	ret void

}

define void @s1() {
l3:
	call void @eqI_b(i64 0, i64 0)
	%l5 = call i1 @peek_b_b()
	br i1 %l5, label %case.1, label %l4

l4:
	call void @eqI_b(i64 1, i64 0)
	%l7 = call i1 @peek_b_b()
	br i1 %l7, label %case.2, label %l6

l6:
	br label %case.3

case.1:
	call void @pop_b(i64 1)
	call void @pushI(i64 1)
	ret void

case.2:
	call void @pop_b(i64 1)
	call void @pushI(i64 1)
	ret void

case.3:
	call void @pushI(i64 2)
	call void @push_b(i64 1)
	call void @subI()
	call void @s1()
	call void @pushI(i64 1)
	call void @push_b(i64 2)
	call void @subI()
	call void @s1()
	call void @update_b(i64 1, i64 2)
	call void @updatepop_b(i64 0, i64 1)
	call void @addI()
	ret void

}
